﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UndertaleTextboxSim
{
    class Program
    {
        static void Main(string[] args)
        {
            var cb = new ConversationBuilder("Ralsei", 1);
            cb.Add(Loc.Get("test_convo_ralsei_1"));
            cb.Add(Loc.Get("test_convo_ralsei_2"), 2);
            cb.Add(Loc.Get("test_convo_ralsei_3"), 3);
            cb.Add(Loc.Get("test_convo_ralsei_4"), 4);

            cb.Add(Loc.Get("test_convo_susie_1"), "Susie");
            cb.Add(Loc.Get("test_convo_susie_2"), 9);

            cb.Add(Loc.Get("test_convo_ralsei_5"), "Ralsei", 3);
            cb.Add(Loc.Get("test_convo_ralsei_6"), 4);

            ParseConversation(cb.GetConversation());
        }

        private static void ParseConversation(IEnumerable<Message> conversation)
        {
            foreach (var message in conversation)
            {
                Console.WriteLine($"* {message.Speaker}({message.Expression}): {message.Text}");
            }
            Console.ReadKey();
        }

        private static ConsoleKey GetInput()
        {
            return Console.KeyAvailable
                ? Console.ReadKey(true).Key
                : ConsoleKey.NoName;
        }
    }
}
