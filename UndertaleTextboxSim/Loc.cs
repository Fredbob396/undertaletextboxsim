﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace UndertaleTextboxSim
{
    /// <summary>
    /// Localization Handler
    /// </summary>
    public class Loc
    {
        private static readonly Dictionary<string, string> LocList;

        static Loc()
        {
            var serializer = new JsonSerializer();
            using (var file = File.OpenText("Localization.json"))
            using (var reader = new JsonTextReader(file))
            {
                LocList = serializer.Deserialize<Dictionary<string, string>>(reader);
            }
        }

        /// <summary>
        /// Get the localization value given its key
        /// </summary>
        public static string Get(string key)
        {
            return LocList.TryGetValue(key, out string value)
                ? value
                : "LOCALIZATION ERROR!";
        }
    }
}
