﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UndertaleTextboxSim
{
    public interface IConversationBuilder
    {
        List<Message> GetConversation();
        void Add(string text);
        void Add(string text, bool autoSkip);

        void Add(string text, int expression);
        void Add(string text, int expression, bool autoSkip);

        void Add(string text, string speaker);
        void Add(string text, string speaker, int expression);
        void Add(string text, string speaker, int expression, bool autoSkip);
    }
}
