﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UndertaleTextboxSim
{
    public class Message
    {
        public string Text { get; set; }
        public string Speaker { get; set; }
        public int Expression { get; set; }
        public bool AutoSkip { get; set; }
    }
}
