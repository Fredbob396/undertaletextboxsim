﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UndertaleTextboxSim
{
    public class ConversationBuilder : IConversationBuilder
    {
        private string _speaker;
        private int _expression;
        private bool _autoSkip;
        private readonly List<Message> _conversation;

        public ConversationBuilder(string speaker = "", int expression = 0, bool autoSkip = false)
        {
            _speaker = speaker;
            _expression = expression;
            _autoSkip = autoSkip;
            _conversation = new List<Message>();
        }

        public List<Message> GetConversation()
        {
            return _conversation;
        }

        public void Add(string text)
        {
            _conversation.Add(new Message
            {
                Text = text,
                Speaker = _speaker,
                Expression = _expression,
                AutoSkip = _autoSkip
            });
        }

        public void Add(string text, bool autoSkip)
        {
            _autoSkip = autoSkip;
            _conversation.Add(new Message
            {
                Text = text,
                Speaker = _speaker,
                Expression = _expression,
                AutoSkip = autoSkip
            });
        }

        public void Add(string text, int expression)
        {
            _expression = expression;
            _conversation.Add(new Message
            {
                Text = text,
                Speaker = _speaker,
                Expression = expression,
                AutoSkip = _autoSkip
            });
        }

        public void Add(string text, int expression, bool autoSkip)
        {
            _expression = expression;
            _autoSkip = autoSkip;
            _conversation.Add(new Message
            {
                Text = text,
                Speaker = _speaker,
                Expression = expression,
                AutoSkip = autoSkip
            });
        }

        public void Add(string text, string speaker)
        {
            _speaker = speaker;
            _conversation.Add(new Message
            {
                Text = text,
                Speaker = speaker,
                Expression = _expression,
                AutoSkip = _autoSkip
            });
        }

        public void Add(string text, string speaker, int expression)
        {
            _speaker = speaker;
            _expression = expression;
            _conversation.Add(new Message
            {
                Text = text,
                Speaker = speaker,
                Expression = expression,
                AutoSkip = _autoSkip
            });
        }

        public void Add(string text, string speaker, int expression, bool autoSkip)
        {
            _speaker = speaker;
            _expression = expression;
            _autoSkip = autoSkip;
            _conversation.Add(new Message
            {
                Text = text,
                Speaker = speaker,
                Expression = expression,
                AutoSkip = autoSkip
            });
        }
    }
}
